La idea de esta guía es que sea un documento vivo. Utilizamos la misma metodología como si estuvieses contribuyendo a un proyecto Open Source.

# Cambios al contenido
Si quieres contribuir al contenido (i.e. agregando algo), puedes hacer un [Merge Request](https://gitlab.com/rburhum/startupperu/merge_requests) con los cambios. 

# Sugerencias 
Tambien puedes dar recomendaciones de temas que crees que necesitan expansión o que hemos omitido. Crea [un Issue](https://gitlab.com/rburhum/startupperu/issues) con los detalles.