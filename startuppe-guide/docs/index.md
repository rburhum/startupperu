#Guía No-Oficial para Startups Peruanas
![Unicorn](resources/unicorn.jpg)

##Bienvenido emprendedor!

La siguiente guía es un documento informal - realmente un borrador - que este [humilde autor](http://blog.burhum.com/aboutme) está recompilando con el fin de dar su granito de arena al ecosistema peruano. 

Si estas pensando en crear un startup y tienes preguntas como

 - ¿Qué es un startup?
 - ¿Cómo empezar?
 - ¿Cómo consigo financiamiento?
 - ¿Cómo armo un equipo?

espero hayas llegado al lugar correcto - ya que el objetivo de este website es de quitar la mítica alrededor de ese tipo de preguntas.

!!! OJO
    Si estas buscando la página oficial del programa Startup Perú del Ministerio de Producción, estas en el lugar equivocado. [Quieres ir a este website](http://www.start-up.pe) en vez.



Si estas listo para empezar, el primer paso va a ser "hacer la tarea"


Este un documento *vivo* (i.e. que evoluciona *constantemente*) con todo el contenido en un [repositorio abierto](https://gitlab.com/rburhum/startupperu) al cuál puedes [contribuir](https://gitlab.com/rburhum/startupperu/blob/master/CONTRIBUTING.md) con cambios y sugerencias.
