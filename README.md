# StartupPeru.pe

Contenido de StartupPeru.pe

# Contribuciones

Si quieres ayudar a mejorar este contenido, [lee el archivo CONTRIBUTING.md](https://gitlab.com/rburhum/startupperu/blob/master/CONTRIBUTING.md)

# Correr el servidor local
```bash
pip install -r requirements.txt
cd startuppe-guide/
mkdoc serve
```

# test builds
```bash
gitlab-runner exec docker --cache-dir=/tmp/gitlabrunner --docker-volumes /tmp/gitlabrunner:/builds pages
```
